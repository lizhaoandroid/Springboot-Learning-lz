package com.lizhaoblog.pro008package;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Pro008PackageApplication {

    public static void main(String[] args) {
        SpringApplication.run(Pro008PackageApplication.class, args);
    }
}
