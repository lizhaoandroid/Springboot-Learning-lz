/*
 * Copyright (C), 2015-2018
 * FileName: User
 * Author:   zhao
 * Date:     2018/10/11 17:51
 * Description: User实体类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.pro005commonmybatis.entity;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 〈一句话功能简述〉<br>
 * 〈User实体类〉
 *
 * @author zhao
 * @date 2018/10/11 17:51
 * @since 1.0.1
 */
@Table(name = "user")
public class User {
  @Id
  private Integer id;
  private String name;
  private Integer age;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  @Override
  public String toString() {
    return "User{" + "id=" + id + ", name='" + name + '\'' + ", age=" + age + '}';
  }
}
