/*
 * Copyright (C), 2015-2018
 * FileName: UserDao
 * Author:   zhao
 * Date:     2018/10/11 20:28
 * Description: Userdao
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.pro005commonmybatis.dao.mysql;

import com.lizhaoblog.pro005commonmybatis.base.mybatis.CrudMapper;
import com.lizhaoblog.pro005commonmybatis.entity.User;

import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈Userdao〉
 *
 * @author zhao
 * @date 2018/10/11 20:28
 * @since 1.0.1
 */
public interface UserDao extends CrudMapper<User, Integer> {
  List<User> findAll();
}
