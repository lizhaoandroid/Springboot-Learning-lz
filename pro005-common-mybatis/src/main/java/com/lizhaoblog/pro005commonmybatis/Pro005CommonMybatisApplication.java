package com.lizhaoblog.pro005commonmybatis;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.lizhaoblog.pro005commonmybatis.dao")
public class Pro005CommonMybatisApplication {

  public static void main(String[] args) {
    SpringApplication.run(Pro005CommonMybatisApplication.class, args);
  }
}
