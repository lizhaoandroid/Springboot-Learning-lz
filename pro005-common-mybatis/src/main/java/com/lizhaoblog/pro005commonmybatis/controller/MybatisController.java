/*
 * Copyright (C), 2015-2018
 * FileName: MybatisController
 * Author:   zhao
 * Date:     2018/10/11 18:01
 * Description: MybatisController
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.pro005commonmybatis.controller;

import com.lizhaoblog.pro005commonmybatis.dao.mysql.CategoryDao;
import com.lizhaoblog.pro005commonmybatis.dao.mysql.UserDao;
import com.lizhaoblog.pro005commonmybatis.entity.Category;
import com.lizhaoblog.pro005commonmybatis.entity.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈MybatisController〉
 *
 * @author zhao
 * @date 2018/10/11 18:01
 * @since 1.0.1
 */
@RestController
@RequestMapping("/mybatis")
public class MybatisController {
  @Autowired
  private UserDao userDao;
  @Autowired
  private CategoryDao categoryDao;

  @RequestMapping(value = "/find-all")
  public String findAll() {
    List<User> all = userDao.findAll();
    return all.toString();
  }
  @RequestMapping(value = "/list-all")
  public String listAll() {
    List<User> all = userDao.listAll();
    return all.toString();
  }

  @RequestMapping(value = "/list-all2")
  public String listAll2() {
    List<Category> all = categoryDao.listAll();
    return all.toString();
  }
}
