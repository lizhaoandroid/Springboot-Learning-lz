package com.lizhaoblog.pro010shiro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Pro010ShiroApplication {

    public static void main(String[] args) {
        SpringApplication.run(Pro010ShiroApplication.class, args);
    }
}
