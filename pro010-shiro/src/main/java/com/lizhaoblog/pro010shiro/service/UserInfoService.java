package com.lizhaoblog.pro010shiro.service;

import com.lizhaoblog.pro010shiro.entity.UserInfo;

public interface UserInfoService {
    /**通过username查找用户信息;*/
    public UserInfo findByUsername(String username);
}