package com.lizhaoblog.pro006scheduled;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class Pro006ScheduledApplication {

  public static void main(String[] args) {
    SpringApplication.run(Pro006ScheduledApplication.class, args);
  }
}
