/*
 * Copyright (C), 2015-2018
 * FileName: MyScheduler
 * Author:   zhao
 * Date:     2018/10/12 19:07
 * Description: 测试用的定时任务
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.pro006scheduled.scheduled;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 〈一句话功能简述〉<br>
 * 〈测试用的定时任务〉
 *
 * @author zhao
 * @date 2018/10/12 19:07
 * @since 1.0.1
 */
@Component
public class MyScheduler {

  private static final Logger logger = LoggerFactory.getLogger(MyScheduler.class);

  private int count = 0;
  private int count2 = 0;
  private int count3 = 0;

  @Scheduled(fixedRate = 3000)
  public void fixedRate() {
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      logger.debug("fixedRate", e);
    }
    count++;
    logger.info("fixedRate " + count + " time " + System.currentTimeMillis());
  }

  @Scheduled(fixedDelay = 3000)
  public void fixedDelay() {
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      logger.debug("fixedDelay", e);
    }
    count2++;
    logger.info("fixedDelay " + count2 + " time " + System.currentTimeMillis());
  }

  //  每隔3秒执行一次：*/3 * * * * ?
  @Scheduled(cron = "*/3 * * * * ?")
  public void cron() {
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      logger.debug("cron", e);
    }
    count3++;
    logger.info("cron " + count3 + " time " + System.currentTimeMillis());
  }

}
