package com.lizhaobolg.message.rabbit;

import com.lizhaobolg.message.rabbit.advance.entity.EntitySender;
import com.lizhaobolg.message.rabbit.advance.fanout.FanoutSender;
import com.lizhaobolg.message.rabbit.advance.topic.TopicSender;
import com.lizhaobolg.message.rabbit.easy.Sender;
import com.lizhaobolg.message.rabbit.multimap.ManyToManyReceiver1;
import com.lizhaobolg.message.rabbit.multimap.ManyToManyReceiver2;
import com.lizhaobolg.message.rabbit.multimap.ManyToManyReceiver3;
import com.lizhaobolg.message.rabbit.multimap.ManyToManySender;
import com.lizhaobolg.message.rabbit.multimap.ManyToManySender2;
import com.lizhaobolg.message.rabbit.multimap.OneToManySender;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MessageApplication.class)
public class MessageApplicationTests {

    @Test
    public void contextLoads() {
    }

    @Autowired
    private Sender sender;
    @Autowired
    private OneToManySender oneToManySender;
    @Autowired
    private ManyToManySender manyToManySender;
    @Autowired
    private ManyToManySender2 manyToManySender2;
    @Autowired
    private ManyToManyReceiver1 manyToManyReceiver1;
    @Autowired
    private ManyToManyReceiver2 manyToManyReceiver2;
    @Autowired
    private ManyToManyReceiver3 manyToManyReceiver3;

    @Autowired
    private EntitySender entitySender;
    @Autowired
    private TopicSender topicSender;
    @Autowired
    private FanoutSender fanoutSender;

    // 测试一对一
    @Test
    public void easy() {

        //  结果
        //        Sender : hello Wed Nov 14 19:33:16 GMT+08:00 2018
        //        Receiver : hello Wed Nov 14 19:33:16 GMT+08:00 2018
        sender.send();
        sender.sendFoo();
    }

    @Test
    public void hello() throws Exception {
        for (int i = 0; i < 100; i++) {
            sender.send();
            if (i % 2 == 0) {
                sender.sendFoo();
            }
            Thread.sleep(1000);
        }
    }

    // 测试一对多消息
    @Test
    public void testOneToMany() throws Exception {
        // 一个发送端，2个接收端，可以看到结果是平均分布的
        // 结果
        // OneToManySender : OneToMany Wed Nov 14 19:37:17 GMT+08:00 2018
        //OneToManyReceiver2 : OneToMany Wed Nov 14 19:37:17 GMT+08:00 2018
        //OneToManySender : OneToMany Wed Nov 14 19:37:18 GMT+08:00 2018
        //OneToManyReceiver1 : OneToMany Wed Nov 14 19:37:18 GMT+08:00 2018
        //OneToManySender : OneToMany Wed Nov 14 19:37:18 GMT+08:00 2018
        //OneToManyReceiver2 : OneToMany Wed Nov 14 19:37:18 GMT+08:00 2018
        //OneToManySender : OneToMany Wed Nov 14 19:37:18 GMT+08:00 2018
        //OneToManyReceiver1 : OneToMany Wed Nov 14 19:37:18 GMT+08:00 2018
        //OneToManySender : OneToMany Wed Nov 14 19:37:18 GMT+08:00 2018
        //OneToManyReceiver2 : OneToMany Wed Nov 14 19:37:18 GMT+08:00 2018
        //OneToManySender : OneToMany Wed Nov 14 19:37:18 GMT+08:00 2018
        //OneToManyReceiver1 : OneToMany Wed Nov 14 19:37:18 GMT+08:00 2018
        //OneToManySender : OneToMany Wed Nov 14 19:37:18 GMT+08:00 2018
        //OneToManyReceiver2 : OneToMany Wed Nov 14 19:37:18 GMT+08:00 2018
        for (int i = 0; i < 100; i++) {
            oneToManySender.send();
            Thread.sleep(100);
        }
    }

    // 测试多对多消息
    @Test
    public void testManyToMany() throws Exception {
        //这里是2对3的关系，结果看上去好像不是平均的，
        // 我们加上一个count，来统计各个接收者执行的次数，最后发现是66.67.67,
        // 所以是平均的
        // 结果
        //        manyToManyReceiver1.count: 67
        //        manyToManyReceiver2.count: 66
        //        manyToManyReceiver3.count: 67
        //        ManyToManyReceiver3 : ManyToManySender1 Wed Nov 14 19:40:31 GMT+08:00 2018
        //        ManyToManyReceiver1 : ManyToManySender2 Wed Nov 14 19:40:31 GMT+08:00 2018
        //        ManyToManySender1 : ManyToManySender1 Wed Nov 14 19:40:31 GMT+08:00 2018
        //        ManyToManySender2 : ManyToManySender2 Wed Nov 14 19:40:31 GMT+08:00 2018
        //        ManyToManyReceiver2 : ManyToManySender1 Wed Nov 14 19:40:31 GMT+08:00 2018
        //        ManyToManyReceiver3 : ManyToManySender2 Wed Nov 14 19:40:31 GMT+08:00 2018
        //        ManyToManySender1 : ManyToManySender1 Wed Nov 14 19:40:31 GMT+08:00 2018
        //        ManyToManySender2 : ManyToManySender2 Wed Nov 14 19:40:31 GMT+08:00 2018
        //        ManyToManyReceiver2 : ManyToManySender2 Wed Nov 14 19:40:31 GMT+08:00 2018
        //        ManyToManyReceiver1 : ManyToManySender1 Wed Nov 14 19:40:31 GMT+08:00 2018
        //        ManyToManySender1 : ManyToManySender1 Wed Nov 14 19:40:31 GMT+08:00 2018
        //        ManyToManySender2 : ManyToManySender2 Wed Nov 14 19:40:31 GMT+08:00 2018
        //        ManyToManyReceiver1 : ManyToManySender2 Wed Nov 14 19:40:31 GMT+08:00 2018
        //        ManyToManyReceiver3 : ManyToManySender1 Wed Nov 14 19:40:31 GMT+08:00 2018

        for (int i = 0; i < 100; i++) {
            manyToManySender.send();
            manyToManySender2.send();
            Thread.sleep(100);
        }
        System.out.println(
                "manyToManyReceiver1.count: " + manyToManyReceiver1.count + "\n" + "manyToManyReceiver2.count: "
                        + manyToManyReceiver2.count + "\n" + "manyToManyReceiver3.count: " + manyToManyReceiver3.count
                        + "\n");

    }

    // 测试发送实体
    @Test
    public void entity() {
        // 让实体实现Serializable接口，就能直接发送了
        // 结果
        // Sender : User{id=1, name='小王'}
        // Receiver : User{id=1, name='小王'}
        entitySender.send();
    }

    // topic -- 测试根据key来绑定队列
    @Test
    public void topic() {
        // 这里queueMessages这个队列，可以被2个key匹配，
        // 本身topic.messages会绑定过一个队列
        // 所以会执行2次topic.messages的消息

        // 结果
        // Sender : hi, i am message 1
        // Sender : hi, i am messages 2
        // TopicReceiver2 : hi, i am message 1
        // TopicReceiver1 : hi, i am message 1
        // TopicReceiver2 : hi, i am messages 2

        topicSender.send1();
        topicSender.send2();
    }

    // fanout -- 广播机制
    @Test
    public void fanout() {
        // 这里属于广播，所以发送一次，三个客户端都能接收到
        // 结果
        // Sender : hi, fanout msg
        // FanoutReceiverB : hi, fanout msg
        // FanoutReceiverC : hi, fanout msg
        // FanoutReceiverA : hi, fanout msg
        fanoutSender.send1();
    }

}
