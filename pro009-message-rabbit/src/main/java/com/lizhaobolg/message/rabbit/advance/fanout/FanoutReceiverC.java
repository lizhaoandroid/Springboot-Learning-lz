/*
 * Copyright (C), 2015-2018
 * FileName: FanoutReceiver
 * Author:   zhao
 * Date:     2018/11/14 18:58
 * Description: fanout消费者
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaobolg.message.rabbit.advance.fanout;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 〈一句话功能简述〉<br>
 * 〈fanout消费者〉
 *
 * @author zhao
 * @date 2018/11/14 18:58
 * @since 1.0.1
 */
@Component
@RabbitListener(queues = "fanout.C")
public class FanoutReceiverC {

    @RabbitHandler
    public void process(String msg) {
        System.out.println("FanoutReceiverC : " + msg);
    }

}
