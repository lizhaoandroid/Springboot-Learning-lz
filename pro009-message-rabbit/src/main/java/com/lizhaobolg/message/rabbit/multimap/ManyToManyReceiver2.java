/*
 * Copyright (C), 2015-2018
 * FileName: ManyToManyReceiver2
 * Author:   zhao
 * Date:     2018/11/14 16:59
 * Description: 多对多接收者2
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaobolg.message.rabbit.multimap;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 〈一句话功能简述〉<br>
 * 〈多对多接收者2〉
 *
 * @author zhao
 * @date 2018/11/14 16:59
 * @since 1.0.1
 */
@Component
@RabbitListener(queues = "manyToMany")
public class ManyToManyReceiver2 {

    public int count = 0;
    @RabbitHandler
    public void process(String foo) {
        System.out.println("ManyToManyReceiver2 : " + foo);
        count++;
    }

}
