/*
 * Copyright (C), 2015-2018
 * FileName: FooReceiver
 * Author:   zhao
 * Date:     2018/11/14 15:45
 * Description: foo消费者
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaobolg.message.rabbit.easy;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 〈一句话功能简述〉<br>
 * 〈foo消费者〉
 *
 * @author zhao
 * @date 2018/11/14 15:45
 * @since 1.0.1
 */
@Component
@RabbitListener(queues = "foo")
public class FooReceiver {

    @RabbitHandler
    public void process(String foo) {
        System.out.println("FooReceiver : " + foo);
    }

}
