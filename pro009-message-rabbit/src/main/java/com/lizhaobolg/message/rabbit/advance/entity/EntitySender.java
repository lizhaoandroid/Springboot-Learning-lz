/*
 * Copyright (C), 2015-2018
 * FileName: Sender
 * Author:   zhao
 * Date:     2018/11/14 15:28
 * Description: 发送者
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaobolg.message.rabbit.advance.entity;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 〈一句话功能简述〉<br>
 * 〈发送者〉
 *
 * @author zhao
 * @date 2018/11/14 15:28
 * @since 1.0.1
 */
@Component
public class EntitySender {

    @Autowired
    private AmqpTemplate rabbitTemplate;

    public void send() {
        User user = new User(1, "小王");
        System.out.println("Sender : " + user);
        rabbitTemplate.convertAndSend("entity", user);
    }

}
