/*
 * Copyright (C), 2015-2018
 * FileName: Receiver
 * Author:   zhao
 * Date:     2018/11/14 15:28
 * Description: 消费者
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaobolg.message.rabbit.advance.entity;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 〈一句话功能简述〉<br>
 * 〈消费者〉
 *
 * @author zhao
 * @date 2018/11/14 15:28
 * @since 1.0.1
 */
@Component
@RabbitListener(queues = "entity")
public class EntityReceiver {

    @RabbitHandler
    public void process(User user) {
        System.out.println("Receiver : " + user.toString());
    }

}
