/*
 * Copyright (C), 2015-2018
 * FileName: one2ManySender
 * Author:   zhao
 * Date:     2018/11/14 16:57
 * Description: 一对多翻译
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaobolg.message.rabbit.multimap;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 〈一句话功能简述〉<br>
 * 〈一对多翻译〉
 *
 * @author zhao
 * @date 2018/11/14 16:57
 * @since 1.0.1
 */
@Component
public class OneToManySender {
    @Autowired
    private AmqpTemplate rabbitTemplate;

    public void send() {
        String context = "OneToMany " + new Date();
        System.out.println("OneToManySender : " + context);
        rabbitTemplate.convertAndSend("OneToMany", context);
    }

}
