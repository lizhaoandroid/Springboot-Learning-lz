/*
 * Copyright (C), 2015-2018
 * FileName: User
 * Author:   zhao
 * Date:     2018/11/14 18:25
 * Description: 实体--用户
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaobolg.message.rabbit.advance.entity;

import java.io.Serializable;

/**
 * 〈一句话功能简述〉<br>
 * 〈实体--用户〉
 *
 * @author zhao
 * @date 2018/11/14 18:25
 * @since 1.0.1
 */
public class User implements Serializable {


    private int id;
    private String name;

    public User(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", name='" + name + '\'' + '}';
    }
}
