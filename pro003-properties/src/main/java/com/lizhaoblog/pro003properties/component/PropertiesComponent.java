/*
 * Copyright (C), 2015-2018
 * FileName: PropertiesComponent
 * Author:   zhao
 * Date:     2018/10/10 15:17
 * Description: 用于接收配置文件
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.pro003properties.component;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 〈一句话功能简述〉<br>
 * 〈用于接收配置文件〉
 *
 * @author zhao
 * @date 2018/10/10 15:17
 * @since 1.0.1
 */
@Component
public class PropertiesComponent {

  @Value("${com.lizhaoblog.name}")
  private String name;
  @Value("${com.lizhaoblog.title}")
  private String title;
  @Value("${com.lizhaoblog.desc}")
  private String desc;
  @Value("${com.lizhaoblog.mode}")
  private String mode;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDesc() {
    return desc;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }

  public String getMode() {
    return mode;
  }

  public void setMode(String mode) {
    this.mode = mode;
  }
}
