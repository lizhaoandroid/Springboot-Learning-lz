/*
 * Copyright (C), 2015-2018
 * FileName: PropertiesController
 * Author:   zhao
 * Date:     2018/10/10 15:11
 * Description: 用于测试配置文件
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.pro003properties.controller;

import com.lizhaoblog.pro003properties.component.PropertiesComponent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 〈一句话功能简述〉<br>
 * 〈用于测试配置文件〉
 *
 * @author zhao
 * @date 2018/10/10 15:11
 * @since 1.0.1
 */
@RestController
public class PropertiesController {

  @Autowired
  private PropertiesComponent propertiesComponent;

  @Value("${com.lizhaoblog.useByController}")
  private Integer useByController;


  @RequestMapping(value = "/hello")
  public String index() {
    String result = "";

    result += propertiesComponent.getName();
    result += "<br />";
    result += propertiesComponent.getTitle();
    result += "<br />";
    result += propertiesComponent.getDesc();
    result += "<br />";
    result += propertiesComponent.getMode();
    result += "<br />";
    result += useByController;
    result += "<br />";

    return result;
  }
}
