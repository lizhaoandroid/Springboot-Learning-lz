package com.lizhaoblog.pro003properties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Pro003PropertiesApplication {

  public static void main(String[] args) {
    SpringApplication.run(Pro003PropertiesApplication.class, args);
  }
}
