/*
 * Copyright (C), 2015-2018
 * FileName: UserDao
 * Author:   zhao
 * Date:     2018/10/11 17:50
 * Description: User数据库连接类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.pro004mybatis.dao.mysql;

import com.lizhaoblog.pro004mybatis.entity.User;

import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈User数据库连接类〉
 *
 * @author zhao
 * @date 2018/10/11 17:50
 * @since 1.0.1
 */
public interface UserDao {
  List<User> findAll();
}
