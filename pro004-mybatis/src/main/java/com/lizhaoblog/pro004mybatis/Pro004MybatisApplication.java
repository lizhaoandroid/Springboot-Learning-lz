package com.lizhaoblog.pro004mybatis;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.lizhaoblog.pro004mybatis.dao")
public class Pro004MybatisApplication {

  public static void main(String[] args) {
    SpringApplication.run(Pro004MybatisApplication.class, args);
  }
}
