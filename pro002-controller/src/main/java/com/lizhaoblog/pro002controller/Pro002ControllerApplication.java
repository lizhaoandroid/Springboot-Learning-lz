package com.lizhaoblog.pro002controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Pro002ControllerApplication {

  public static void main(String[] args) {
    SpringApplication.run(Pro002ControllerApplication.class, args);
  }
}
