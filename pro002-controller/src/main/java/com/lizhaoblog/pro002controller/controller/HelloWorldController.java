/*
 * Copyright (C), 2015-2018
 * FileName: HelloWorldController
 * Author:   zhao
 * Date:     2018/10/9 17:53
 * Description: HelloWorld控制器
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.pro002controller.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;

/**
 * 〈一句话功能简述〉<br>
 * 〈HelloWorld控制器〉
 *
 * @author zhao
 * @date 2018/10/9 17:53
 * @since 1.0.1
 */
@RestController
public class HelloWorldController {

  @RequestMapping("/hello")
  public String index() {
    // 这里访问地址是 http://127.0.0.1:8080/hello
    return "Hello World";
  }

  @RequestMapping("/hello2")
  public String hello2() {
    // 这里访问地址是 http://127.0.0.1:8080/hello2
    return "Hello World 2";
  }

  @RequestMapping("/request-param")
  public String requestParam(@RequestParam("id") String id) {
    // 这里访问地址是 http://127.0.0.1:8080/request-param?id=sadho
    return "requestParam id = " + id;
  }

  @RequestMapping("/path-param/{id}")
  public String pathParam(@PathVariable("id") String id) {
    // 这里访问地址是 http://127.0.0.1:8080/path-param/sadho
    return "pathParam id = " + id;
  }

}
