/*
 * Copyright (C), 2015-2018
 * FileName: ViewController
 * Author:   zhao
 * Date:     2018/10/9 19:00
 * Description: 返回网页
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaoblog.pro002controller.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * 〈一句话功能简述〉<br>
 * 〈返回网页〉
 *
 * @author zhao
 * @date 2018/10/9 19:00
 * @since 1.0.1
 */
@Controller
@RequestMapping("/view")
public class ViewController {

  @RequestMapping("/index")
  public String index() {
    // 返回的是一个静态页面
    // 对应static/view/index.html
    // 这里访问地址是 http://127.0.0.1:8080/view/index
    return "index.html";
  }


  @RequestMapping("/login")
  public String login() {
    // 返回的是一个静态页面
    // 对应static/login.html
    // 这里访问地址是 http://127.0.0.1:8080/view/login
    return "/login.html";
  }

  @RequestMapping("/freemarker")
  public ModelAndView freemarker() {

    // 返回的是一个模板页面(动态)
    // 这里访问地址是 http://127.0.0.1:8080/view/freemarker
    ModelAndView mav = new ModelAndView();
    mav.addObject("message", "SpringBoot freemarker！");
    mav.setViewName("freemarkerview");
    return mav;
  }


  @ResponseBody
  @RequestMapping("/stringbuf")
  public String stringbuf() {
    // 这里访问地址是 http://127.0.0.1:8080/view/stringbuf
    return "index.html";
  }

}
