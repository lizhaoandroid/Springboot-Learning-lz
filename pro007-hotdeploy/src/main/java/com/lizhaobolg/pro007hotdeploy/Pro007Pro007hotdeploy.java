/*
 * Copyright (C), 2015-2018
 * FileName: Pro007PackageApplication
 * Author:   zhao
 * Date:     2018/11/10 14:18
 * Description: 启动类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.lizhaobolg.pro007hotdeploy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 〈一句话功能简述〉<br>
 * 〈启动类〉
 *
 * @author zhao
 * @date 2018/11/10 14:18
 * @since 1.0.1
 */
@SpringBootApplication
@EnableScheduling
public class Pro007Pro007hotdeploy {

    public static void main(String[] args) {
        SpringApplication.run(Pro007Pro007hotdeploy.class, args);
    }
}